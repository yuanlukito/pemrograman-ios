//
//  ViewController.swift
//  Hello World
//
//  Created by Yuan Lukito on 1/21/17.
//  Copyright © 2017 FTI UKDW. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showHelloWorld(_ sender: UIButton) {
        //buat object alert
        let alert = UIAlertController(title: "Hello World!", message: "Selamat belajar iOS Programming!", preferredStyle: .alert)
        
        //buat object alertaction (berupa button)
        let buttonAlertOk = UIAlertAction(title: "OK. Good luck!", style: .cancel, handler: nil)
        
        //tambahkan buttonAlertOk ke alert
        alert.addAction(buttonAlertOk)
        
        //tampilkan alert
        self.show(alert, sender: self)
        
        //menampilkan tulisan di console (untuk log, debugging atau keperluan lainnya)
        print("Hello World! button was pressed.")
    }

}

