# README #

Repository ini berisi materi kuliah Pemrograman iOS (TIP333) yang diajarkan di Teknik Informatika, Universitas Kristen Duta Wacana. Saat ini sedang berjalan semester genap 2016/2017.

Repository ini terbuka untuk umum dan dapat dimodifikasi/diubah sesuai dengan kebutuhan anda masing-masing.  Semua saran dan masukan untuk pengembangan materi ini akan diterima dengan senang hati.

### DESKRIPSI MATA KULIAH ###

Mata kuliah ini membahas mengenai metode-metode pengembangan aplikasi untuk platform iOS menggunakan bahasa pemrograman Swift.

### DOSEN PENGAMPU ###

Yuan Lukito, S.Kom., M.Cs (Teknik Informatika UKDW).