//
//  ViewController.swift
//  Keyboard
//
//  Created by Yuan Lukito on 3/9/17.
//  Copyright © 2017 FTI UKDW. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func dismissKeyboard(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

}

