//
//  ViewController.swift
//  Quiz
//
//  Created by Yuan Lukito on 2/23/17.
//  Copyright © 2017 FTI UKDW. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var score:Int = 0
    var quiz = QuizModel()
    
    @IBOutlet weak var labelScore: UILabel!
    
    @IBOutlet weak var labelQuestion: UILabel!
    
    @IBOutlet weak var segmentedAnswer: UISegmentedControl!
    
    @IBAction func answer(_ sender: UISegmentedControl) {
        let ans = segmentedAnswer.selectedSegmentIndex
        if ans == quiz.key {
            score += 4
        }
        else {
            score -= 1
        }
        //update score
        labelScore.text = "\(score)"
        
        //generate soal baru
        quiz = QuizModel()
        labelQuestion.text = quiz.question
        labelScore.text = "\(score)"
        //tampilkan jawaban
        for i in 0...3 {
            segmentedAnswer.setTitle(quiz.answers[i], forSegmentAt: i)
            segmentedAnswer.selectedSegmentIndex = UISegmentedControlNoSegment
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tampilkan soal dan score
        labelQuestion.text = quiz.question
        labelScore.text = "\(score)"
        //tampilkan jawaban
        for i in 0...3 {
            segmentedAnswer.setTitle(quiz.answers[i], forSegmentAt: i)
        }
    }


}

