//
//  ViewController.swift
//  Label
//
//  Created by Yuan Lukito on 4/6/17.
//  Copyright © 2017 Teknik Informatika UKDW. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var labelDisplay: UILabel!
    
    var data = Data(text:"")
    
    @IBAction func unwindSegue(segue:UIStoryboardSegue) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        data = Data(text: "Hello World!")
        labelDisplay.text = data.text
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        labelDisplay.text = data.text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showUpdateLabel" {
            let dest = segue.destination as! LabelUpdateViewController
            dest.data = data
        }
    }

}

