//: Playground - noun: a place where people can play

import UIKit
enum Suit:Int {
    case Hearts = 1
    case Spades, Diamonds, Clubs
    
    func toString() -> String {
        switch(self) {
        case .Clubs: return "♣️"
        case .Hearts: return "❤️"
        case .Spades: return "♠️"
        case .Diamonds: return "♦️"
        }
    }
}

enum Rank:Int {
    case Ace = 1
    case Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King
    
    func toString() -> String {
        switch(self) {
        case .Ace: return "A"
        case .Jack: return "J"
        case .Queen: return "Q"
        case .King: return "K"
        default: return String(self.rawValue)
        }
    }
}

class Card {
    var suit:Suit
    var rank:Rank
    
    //constructor default
    init(suit:Suit, rank:Rank){
        self.suit = suit
        self.rank = rank
    }
    //constructor random card
    init() {
        self.suit = Suit(rawValue: Int((arc4random() % 4) + 1))!
        self.rank = Rank(rawValue: Int((arc4random() % 13) + 1))!
    }
    
    init(suit:Suit) {
        self.suit = suit
        self.rank = Rank(rawValue: Int((arc4random() % 13) + 1))!
    }
    
    func show() {
        print("\(self.rank.toString()) \(self.suit.toString())")
    }
    
    func compare(otherCard:Card) -> Int {
        return self.rank.rawValue - otherCard.rank.rawValue
    }
}

class Deck {
    var cards:[Card]
    
    init (isRandomized: Bool = true) {
        cards = [Card]()
        for suit in 1...4 {
            for rank in 1...13 {
                let newCard = Card(suit: Suit(rawValue: suit)!, rank: Rank(rawValue: rank)!)
                cards.append(newCard)
            }
        }
        if isRandomized {
            for _ in 1...1000 {
                let pos1 = Int(arc4random() % 52)
                let pos2 = Int(arc4random() % 52)
                let card1 = cards[pos1]
                cards[pos1] = cards[pos2]
                cards[pos2] = card1
            }
        }
    }
    
    func countOfCards() -> Int {
        return cards.count
    }
    
    func drawCard() -> Card {
        let card = cards.removeLast()
        return card
    }
}

var myDeck = Deck()
let ghost = Card(suit: .Clubs, rank: .Ace)
repeat {
    var cardOne = myDeck.drawCard()
    cardOne.show()
    if cardOne.rank.rawValue == ghost.rank.rawValue && cardOne.suit.rawValue == ghost.suit.rawValue {
        print("Player one lose!")
        break
    }
    cardOne = myDeck.drawCard()
    cardOne.show()
    if cardOne.rank.rawValue == ghost.rank.rawValue && cardOne.suit.rawValue == ghost.suit.rawValue {
        print("Player two lose!")
        break
    }
}while true

func swapCard(card1: Card, card2:Card) {
    let temp = Card(suit: card1.suit, rank: card1.rank)
    
    card1.suit = card2.suit
    card1.rank = card2.rank
    
    card2.suit = temp.suit
    card2.rank = temp.rank
}

let cardOne = Card()
cardOne.show()
let cardTwo = Card()
cardTwo.show()
swapCard(card1: cardOne, card2: cardTwo)

print("===== setelah ditukar =======")
cardOne.show()
cardTwo.show()

//optionals

class Person {
    var firstName:String
    var middleName:String?
    var lastName:String?
    
    init(first:String, middle:String?, last:String?) {
        firstName = first
        middleName = middle
        lastName = last
    }
    
    func fullName() -> String {
        if let middle = middleName {
            return firstName + " " + middle + lastName!
        }
        else {
            if let last = lastName {
                return firstName + " " + last
            }
            else {
                return firstName
            }
        }
    }
}

let willy = Person(first: "Willy", middle: "Sudiarto", last: "Raharjo")

let dito = Person(first: "Restyandito", middle: nil, last: nil)

let yuan = Person(first: "Yuan", middle: nil, last: "Lukito")

willy.fullName()
dito.fullName()
yuan.fullName()

var a:Int! = 100
if let nilaiA = a {
    print(nilaiA)
}
else {
    print("nil")
}

let c = a + 100










