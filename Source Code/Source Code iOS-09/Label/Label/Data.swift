//
//  Data.swift
//  Label
//
//  Created by Yuan Lukito on 4/6/17.
//  Copyright © 2017 Teknik Informatika UKDW. All rights reserved.
//

import Foundation

class Data {
    var text:String
    init(text:String) {
        self.text = text
    }
}
