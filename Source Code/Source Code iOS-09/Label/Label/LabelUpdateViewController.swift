//
//  LabelUpdateViewController.swift
//  Label
//
//  Created by Yuan Lukito on 4/6/17.
//  Copyright © 2017 Teknik Informatika UKDW. All rights reserved.
//

import UIKit

class LabelUpdateViewController: UIViewController {

    @IBOutlet weak var textUpdate: UITextField!
    
    var data:Data = Data(text: "")
    
    @IBAction func updateLabel(_ sender: UIButton) {
        data.text = textUpdate.text!
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        textUpdate.text = data.text
    }
}
