//
//  ViewController.swift
//  Color Preview
//
//  Created by Yuan Lukito on 3/9/17.
//  Copyright © 2017 FTI UKDW. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var labelPreview: UILabel!

    @IBOutlet weak var sliderRed: UISlider!
    
    @IBOutlet weak var sliderGreen: UISlider!
    
    @IBOutlet weak var sliderBlue: UISlider!
    
    @IBOutlet weak var labelHexBackground: UILabel!
    
    @IBOutlet weak var labelHexText: UILabel!
    
    @IBOutlet weak var segmentedOption: UISegmentedControl!
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        //baca red, green, blue
        let hexString = String(format:"#%02X%02X%02X", arguments:[Int(sliderRed.value), Int(sliderGreen.value), Int(sliderBlue.value)])
        
        let red = sliderRed.value/255.0
        let green = sliderGreen.value/255.0
        let blue = sliderBlue.value/255.0
        
        let newColor = UIColor(colorLiteralRed: red, green: green, blue: blue, alpha: 1.0)
        
        //baca opsi yang terpilih di segmented dan ubah property label
        if segmentedOption.selectedSegmentIndex == 0 {
            //ubah background
            labelPreview.backgroundColor = newColor
            labelHexBackground.text = hexString
        }
        else {
            //ubah text color
            labelPreview.textColor = newColor
            labelHexText.text = hexString
        }
    }
    
    
    @IBAction func segmentedValueChanged(_ sender: UISegmentedControl) {
        //ambil warna sekarang
        var currentColor = labelPreview.backgroundColor
        if segmentedOption.selectedSegmentIndex == 1 {
            currentColor = labelPreview.textColor
        }
        var red:CGFloat = 0
        var green:CGFloat = 0
        var blue:CGFloat = 0
        var alpha:CGFloat = 0
        currentColor?.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        //update nilai slider
        sliderRed.value = Float(red * 255)
        sliderGreen.value = Float(green * 255)
        sliderBlue.value = Float(blue * 255)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}

