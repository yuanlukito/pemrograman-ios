//: Playground - noun: a place where people can play

import UIKit

enum Suit:Int {
    case Hearts = 1, Spades, Diamonds, Clubs
}

enum Rank:Int {
    case Ace = 1, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King
}

struct Card {
    var suit:Suit
    var rank:Rank
    
    init(suit:Suit, rank:Rank) {
        self.suit = suit
        self.rank = rank
    }
    
    func show() {
        print("Card : \(rank) of \(suit)")
    }
    
    //hanya membandingkan berdasarkan nilai kartu saja
    func compare(otherCard: Card)->Int {
        if self.rank.rawValue == otherCard.rank.rawValue {
            return 0
        }
        else if self.rank.rawValue < otherCard.rank.rawValue {
            return -1
        }
        else {
            return 1
        }
    }
}

var aceofhearts = Card(suit: .Hearts, rank: .Ace)
aceofhearts.show()
