//
//  ViewController.swift
//  Prime
//
//  Created by Yuan Lukito on 4/6/17.
//  Copyright © 2017 Teknik Informatika UKDW. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textNumber: UITextField!
    
    @IBAction func unwindSegue(segue:UIStoryboardSegue) {
        //saat kembali, clear input
        self.textNumber.text = ""
    }
    
    @IBAction func checkPrime(_ sender: UIButton) {
        //jika input valid, panggil segue
        if Int(textNumber.text!) != nil{
            performSegue(withIdentifier: "seguePrime", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "seguePrime" {
            //variabel yang akan dikirim
            let input = Int(textNumber.text!)
            //kirim ke tujuan
            let dest = segue.destination as! PrimeCheckerViewController
            dest.inputUser = input!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

