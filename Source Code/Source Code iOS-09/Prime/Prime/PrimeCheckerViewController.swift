//
//  PrimeCheckerViewController.swift
//  Prime
//
//  Created by Yuan Lukito on 4/6/17.
//  Copyright © 2017 Teknik Informatika UKDW. All rights reserved.
//

import UIKit

class PrimeCheckerViewController: UIViewController {
    
    var inputUser:Int = 0
    
    @IBOutlet weak var labelInputUser: UILabel!
    
    @IBOutlet weak var labelResult: UILabel!
    
    func isPrime(input:Int) -> Bool {
        var pembagi = 0
        for i in 1...input {
            if input % i == 0 {
                pembagi += 1
            }
        }
        return pembagi == 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelInputUser.text = String(inputUser)
        if isPrime(input: inputUser) {
            labelResult.text = "PRIME"
            self.view.backgroundColor = UIColor.green
        }
        else {
            labelResult.text = "IS NOT PRIME"
            self.view.backgroundColor = UIColor.red
        }
    }
}
