//: Playground - noun: a place where people can play

import UIKit

/*
 
 FUNGSI
 
 */

//mendefinisikan sebuah fungsi
//- membutuhkan 4 parameter
//- mengembalikan sebuah nilai Int
func max(a:Int, b:Int, c:Int, d:Int) -> Int {
    var biggest = a
    if b > biggest {
        biggest = b
    }
    if c > biggest {
        biggest = c
    }
    if d > biggest {
        biggest = d
    }
    return biggest
}

//menggunakan fungsi tersebut
var result = max(a: 20, b: 15, c: 13, d: 82)

//mendefinisikan fungsi yang memiliki parameter dengan nilai default
func tampilkanBunga(bunga: Float = 0.1) {
    print("Bunga: \(bunga * 100) %")
}

//memanggil fungsi tersebut tanpa parameter
tampilkanBunga()

//memanggil fungsi tersebut dengan parameter
tampilkanBunga(bunga: 0.4)

//fungsi dengan variadic parameters
func mean(data:Int...) -> Float {
    var total = 0
    //ada berapa nilai dalam variabel data?
    let count = data.count
    
    for number in data {
        total = total + number
    }
    
    return Float(total)/Float(count)
}

//pemanggilan fungsi mean
mean(data: 20, 25, 15, 24, 18, 22, 29, 18)
mean(data: 18, 21, 23, 16, 19, 20, 21, 19, 17, 15, 14)
mean(data: 1)

//mendefinisikan fungsi yang memiliki return value lebih dari satu
func minAndMax(data:Int...) -> (max:Int, min:Int) {
    var min = Int.max
    var max = Int.min
    for number in data {
        if number > max {
            max = number
        }
        if number < min {
            min = number
        }
    }
    return (max, min)
}

//memanggil fungsi minAndMax
var hasil = minAndMax(data: 20, 30, 10, 15, 40, 50, 35)
print(hasil.min)
print(hasil.max)

/*
 
 SWITCH
 
 */

//switch-case mirip seperti c/c++/c#
//pada Swift secara default switch-case tidak ada fallthrough
let suit = 1
switch(suit) {
    case 1: print("Hearts")
    case 2: print("Spades")
    case 3: print("Diamonds")
    case 4: print("Clubs")
    default: print("Unknown")
}

//switch-case pada swift bisa menggunakan interval
let nilaiAkhir = 70
switch(nilaiAkhir) {
    case 85...100: print("A")
    case 80..<85: print("A-")
    case 75..<80: print("B+")
    case 70..<75: print("B")
    case 65..<70: print("B-")
    case 60..<65: print("C+")
    case 55..<60: print("C")
    default: print("E")
}

//bisa juga untuk tipe data Float dan Double
let nilaiAkhirSaya:Float = 54.99
switch(nilaiAkhirSaya) {
    case 85...100: print("A")
    case 80..<85: print("A-")
    case 75..<80: print("B+")
    case 70..<75: print("B")
    case 65..<70: print("B-")
    case 60..<65: print("C+")
    case 55..<60: print("C")
    default: print("E")
}

/*
 
 LOOP
 
 */

//looping menggunakan for
for i in 3...8 {
    print("i: \(i)")
}

//decrement
for i in (3...8).reversed() {
    print("i: \(i)")
}

//pada elemen array
let names = ["Iron Man", "Ant Man", "Hulk", "Captain America"]
for name in names {
    print("\(name)")
}

//juga bisa menggunakan while dan repeat-while
var start = 1
while start < 10 {
    print("Start: \(start)")
    start = start + 1
}

var poin = 20
repeat {
    poin = poin + 20
    print("Poin: \(poin)")
} while poin < 100

/*
 
 ARRAY
 
 */

//mendefinisikan sebuah array of integers
var data = [10, 20, 30, 40, 50]
//jumlah elemen dalam suatu array
data.count
//menambahkan elemen ke dalam array, di posisi paling belakang
data.append(60)
//menghapus elemen pada posisi tertentu
data.remove(at: 3)
//menghapus elemen pertama
data.removeFirst()
//membalik isi array
data = data.reversed()
//mengurutkan isi array
data.sort()

//mendefinisikan array kosong yang akan diisi string
var cart = [String]()
cart.append("Eggs")
//mengubah isi elemen index ke-0
cart[0] = "10 Eggs"
cart.append("Milk")
cart.count

//menambahkan elemen ke dalam array (merge array)
cart += ["Cheese"]
cart += ["Butter", "Manggo"]
for item in cart {
    print("\(item)")
}

/*
 
 SET
 
 */

//mendefinisikan set kosong yang akan diisi string
var instruments = Set<String>()
instruments.count
instruments.insert("Guitar")
instruments.insert("Gong")
//masukkan Guitar sekali lagi akan gagal/ditolak karena sudah ada
instruments.insert("Guitar")
instruments.count

//mendefinisikan set dan langsung diisi
var genres:Set<String> = ["Rock", "Pop"]
genres.insert("Dangdut")
genres.insert("Techno")
genres.insert("Jazz")

//menghapus elemen Pop
genres.remove("Pop")

//menampilkan isi set
for genre in genres {
    print("\(genre)")
}

/*
 
 DICTIONARIES
 
 */

//mendefinisikan sebuah dictionary dengan
//key bertipe Int, value bertipe String
var prodi = [Int:String]()
//masukkan kev-value ke dalam dictionary
prodi.updateValue("TI", forKey: 71)
//mengubah value, jika sudah ada
prodi.updateValue("Teknik Informatika", forKey: 71)
prodi.count

//masukkan key-value lagi
prodi.updateValue("Sistem Informasi", forKey: 72)

//bisa juga dengan cara berikut
prodi[73] = "Kedokteran"

//tampilkan isi dictionary
for (kode, nama) in prodi {
    print("Prodi: \(nama) - \(kode)")
}
