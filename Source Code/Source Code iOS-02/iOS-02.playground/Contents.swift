//: Playground - noun: a place where people can play


/*
referensi: 
 
 https://developer.apple.com/library/prerelease/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html

*/

import UIKit

/*
 Variabel dan Konstanta
*/

//konstanta
let pi = 3.14159
let g = 9.8

//ini akan error (tidak bisa diubah nilainya)
//pi = 5.43
//g = 10.3

//variabel
var matakuliah = "Pemrograman Android"

//variabel bisa diganti nilainya
matakuliah = "Pemrograman iOS"

//bisa menggunakan emoji 😎
var mypet = "🐶"
var yourpet = "🐍"

//emoji juga bisa jadi nama variabel
var 🐱 = "cat"
let 🐮 = "cow"

//menampilkan isi variabel
print(matakuliah)
print("\(matakuliah)")

//menampilkan banyak variabel sekaligus
print(mypet + " is better than " + yourpet)
print(mypet, "is better than", yourpet)
print("\(mypet) is better than \(yourpet)")

//mendefinisikan variabel secara "formal"
var campus:String
campus = "UKDW"
print(campus)

var luastanah:Int = 200

//ini akan error
//print("Luas: " + luastanah)

//seharusnya
print("Luas:",luastanah)

//atau
print("Luas: \(luastanah)")

//atau
print("Luas: " + String(luastanah))


//variable type yang umum dipakai:
//Int : bilangan bulat
//Float, Double, Float80 : bilangan pecahan
//Bool : boolean (true/false)
//==========================================
//semuanya adalah Struct

var primapertama = 2

//defaultnya adalah Double
var satukomatiga = 1.3

//bisa dipaksa menjadi Float
var tigakomatiga:Float = 3.3

var ketemu = false
ketemu = true

//Int Merupakan Struct
var nilaiku = 82

//mengakses method advanced dari Int
nilaiku = nilaiku.advanced(by: 8)
print("Nilaiku adalah \(nilaiku)")

//mengakses method distance dari Int
var kurang = nilaiku.distance(to: 100)
print("Kurang \(kurang) untuk menjadi 100")

//mendefinisikan sebuah function
func isPrime(number:Int) -> Bool {
    var divisible = 0
    for i in 1...number{
        if number % i == 0 {
            divisible = divisible + 1
        }
    }
    return divisible == 2
}

//mengakses fungsi isPrime
isPrime(number: 24)
isPrime(number: 29)

//mendefinisikan sebuah struct
struct Barang {
    var nama:String
    var harga:Double
}

//membuat instance dari struct
var sabunMandi = Barang(nama: "Lifebuoy", harga: 1.0)
var sikatGigi = Barang(nama: "OralB", harga: 1.5)

//mengakses property dari struct
print("Nama: \(sabunMandi.nama)")
print("Harga: US$\(sabunMandi.harga)")

//struct adalah value type
var sabunMandi2 = sabunMandi
sabunMandi2.nama = "Dettol"
sabunMandi2.harga = 1.15

print("Nama sabun mandi pertama: \(sabunMandi.nama)")
print("Nama sabun mandi kedua: \(sabunMandi2.nama)")

//mendefinisikan enum
enum Hari:Int {
    case Senin = 1, Selasa, Rabu, Kamis, Jumat, Sabtu, Minggu
}

//mendefinisikan variabel bertipe enum
var awalHariKerja = Hari.Senin
var akhirHariKerja = Hari.Jumat

//bisa juga seperti ini
var kelasIOS = Hari(rawValue: 4)

//membandingkan variabel bertipe enum
var sekarang = Hari.Sabtu
//var sekarang = Hari.Selasa

if sekarang.rawValue >= awalHariKerja.rawValue && sekarang.rawValue <= akhirHariKerja.rawValue {
    print("Selamat bekerja")
}
else {
    print("Selamat berlibur")
}
