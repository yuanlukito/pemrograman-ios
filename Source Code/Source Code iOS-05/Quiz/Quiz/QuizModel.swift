//
//  QuizModel.swift
//  Quiz
//
//  Created by Yuan Lukito on 2/23/17.
//  Copyright © 2017 FTI UKDW. All rights reserved.
//

import Foundation
import UIKit

class QuizModel {
    var question:String
    var answers:[String]
    var key:Int
    
    init() {
        let x = Int(arc4random() % 100)
        let y = Int(arc4random() % 100)
        let z = x + y
        question = "\(x) + \(y) = ?"
        key = Int(arc4random() % 4)
        answers = [String]()
        for i in 0...3 {
            if i == key {
                answers += ["\(z)"]
            }
            else {
                var option = z
                while option == z {
                    option = z + Int(arc4random() % 10) - i
                }
                answers += ["\(option)"]
            }
        }
    }
    
}
